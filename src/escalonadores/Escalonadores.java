/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package escalonadores;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Carolini
 */
public class Escalonadores {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
    Scanner input = new Scanner(System.in);  
        int i, tipoClasse, classe, Proc, contProc = 0,chegada, cpuBurst, prioridade, tempo = 0, ultimaExec, cpuBurstCopia;
        //int[] desempateP = null;
        int contDesem = 0;
        int[] proc;
        
        
        List<Classe> Classes = new ArrayList<>();
        List<Processo> Processos  = new ArrayList<>();
        //List<Processo> Ptabela  = new ArrayList<Processo>();
        List<Processo> PTabela  = new ArrayList<>();
        
        Classe classes = null;
        Processo processo;
        
        
        System.out.print("Entre com o número de classes: \n");  
        classe = input.nextInt();  
        i = 0;
        proc = new int[classe];
       //Setar classes
        while(i < classe){
            System.out.println("******Tipos de classe******* \n");
            System.out.println("0 - First Come First Served (FCFS).\n");
            System.out.println("1 - Shortest Job First (SJF).\n");
            System.out.println("2 - Shortest Remaining Time First (SRTF).\n");
            System.out.println("3 - Escalonamento por Prioridades.\n");
            System.out.println("4 - Escalonamento por Prioridades Preemptivo.\n");
            System.out.println("5 - Round-Robin. \n");
            System.out.println("Informe o tipo da  classe"+i+":\n");
            tipoClasse = input.nextInt(); 
            
            Classes.add(SetarClasse(tipoClasse));
            i++;
        }
        
        //numero de processos por classe
        i = 0;
        while(i < classe){
            System.out.println("Informe o numero de processos da classe"+i+ "\n");
            proc[i] = input.nextInt();
            i++;
        }
        
        //setar classe inferior
        i = 0;
        while(i < classe){
            if((i+1) < classe){
                Classes.get(i).setInferior(Classes.get(i+1));
             }else{
                Classes.get(i).setInferior(null);
             }
            i++;
        }
        
        //Informe as info de cada processo
        i = 0;
        while(i < classe){
            Proc = 0;
            while(Proc < proc[i]){
                contProc = contProc + 1;
                System.out.println("Processo"+contProc+": \n");
                System.out.println("Informe o T. de Chegada: ");
                chegada = input.nextInt();
                System.out.println("Informe o CPU-burst: ");
                cpuBurst = input.nextInt(); 
                System.out.println("Informe o Prioridade: ");
                prioridade = input.nextInt(); 
                System.out.println("\n");
                ultimaExec = chegada;
                cpuBurstCopia = cpuBurst;
                processo = new Processo(contProc, chegada, cpuBurst, prioridade, Classes.get(i), i);
                processo.setUltimaExecucao(ultimaExec);
                processo.setCpuBusrt_copia(cpuBurstCopia);
                Processos.add(processo);
                //PTabela.add(processo);
                Proc++;
            }
            i++;
        }
        
       
        Classe definidas = Classes.get(0);
        Collections.sort(Processos, Processo.ProChegada);
        int Tchegada = 0;
        for(i = 0; i<Processos.size(); i++){
            //System.out.println("chegada: "+Processos.get(i).getChegada()+"\n");
            Tchegada = Tchegada + Processos.get(i).getCpuBusrt();
        }
        //System.out.println("Tchegada: "+Tchegada);
//        tempo = 0;
//        while(Processos.size() > 0){
//         
//                while(Processos.get(0).getChegada() == tempo){
//                    System.out.println("Tempo: "+tempo+"\n");
//                    System.out.println("Tamanho: "+Processos.size()+"\n");
//                    System.out.println("While: "+Processos.get(0).getChegada()+"\n");
//                    Processos.get(0).getClasse().receberProcesso(Processos.get(0));  
//                    Processos.remove(0);
//                    
//                    if(Processos.size() <= 0){
//                        break;
//                    }
//                }
//            
//            tempo++;
//        }
        
        //Exibir classe
        System.out.println("******************Classes definidas******************\n");
        int k = 1; 
        while(definidas != null){
            System.out.print(k+"-");
            definidas.Imprimir();
            definidas = definidas.getInferior();
            k = k +1;
        }
        
        
           System.out.println("******************Diagrama de Gantt******************\n");
           Processo novoProcesso = null, procAnt = null;
           int j = 0;
           tempo = 0;
           novoProcesso = Processos.get(0);
           while(j < Tchegada){ 
               if (Processos.size() > 0){
                    while(Processos.get(0).getChegada() == tempo){
                        
                        //System.out.println("While: "+Processos.get(0).getChegada()+"\n");
                        Processos.get(0).getClasse().receberProcesso(Processos.get(0));  
                        Processos.remove(0);

                        if(Processos.size() <= 0){
                            break;
                        }
                     }
               }
              
                novoProcesso = Classes.get(0).EscolherProcesso(tempo);
               // System.out.println("novoProcesso:"+novoProcesso.getNome());
                
                if(novoProcesso == null && procAnt != null){
                    System.out.print("|");
                }else if(novoProcesso != procAnt){
                    System.out.print("|"+tempo+"-P"+novoProcesso.getNome());
                }else{
                    System.out.print("-");
                }

              tempo++;
              procAnt = novoProcesso;
              j++;
 
           }
           System.out.println(tempo);
           
         // Listb para tabela processos
           Classe tabela = Classes.get(0);
           int tamanho = 0;
           while(tabela != null){
               tamanho = 0;
               while(tamanho < tabela.getFilaAux().size()){
                   PTabela.add(tabela.getFilaAux().get(tamanho));
                   tamanho = tamanho + 1;
               }
               tabela = tabela.inferior;
           }
           
           Collections.sort(PTabela, Processo.ProcNOme);
           
//           for(k=0; k<PTabela.size(); k++){
//               
//               System.out.println("ultima exec: P"+PTabela.get(k).getNome()+ " Ultima exec: "+PTabela.get(k).getUltimaExecucao());
//           }
           
           //Imprimir tabela
           int  e, p;
           float TE = 0, TP = 0;
           System.out.println("******************Tabela de processos******************\n");
           imprimirLinha();
           System.out.println("|Proc   |T.Cheg.  |CPU-b  |Classe |Prior. |T.esp. |T.exec.|\n");
           for(i=0; i<PTabela.size(); i++){
               p =   (int)PTabela.get(i).getUltimaExecucao() - (PTabela.get(i).getChegada()-1);
               e =   (int)( p - PTabela.get(i).getCpuBusrt_copia() );
               TE = TE + e;
               TP = TP + p;
           imprimirLinha();
           System.out.println("|P"+PTabela.get(i).nome+"      |"+PTabela.get(i).chegada+"      |"+PTabela.get(i).cpuBusrt_copia+"      |"+PTabela.get(i).classe.getNome()+"      |"+PTabela.get(i).prioridade+"      |"+e+"      |"+p);
           }
           imprimirLinha();
           TE = (float)(TE/i);
           TP = (float)(TP/i);
           System.out.println("Tempo medio de espera:"+TE+"\n");
           System.out.println("Tempo medio de processamento:"+TP+"\n");
    }
    
   
    public static void imprimirLinha(){
       
           System.out.println("------------------------------------------------------------");
        
    }
    
    public static Classe SetarClasse(Integer tipoClasse){
        int quantun;
        Scanner input = new Scanner(System.in);  
        Classe classe;
        List<Classe> listaClasse = new ArrayList<Classe>();
        String[] algoritmo = {"First Come First Served (FCFS)","Shortest Job First (SJF)",
                              "Shortest Remaining Time First (SRTF)", "Escalonamento por Prioridades", 
                              "Escalonamento por Prioridades Preemptivo", "Round-Robin"};
        
        if(tipoClasse == 5){
               
            classe = new Classe(tipoClasse, algoritmo[tipoClasse]);
            System.out.println("Informe o quantum: ");
            quantun = input.nextInt();
            classe.setQuantum(quantun);
            classe.setQuantum_copia(quantun);
        
        }else{
             classe = new Classe(tipoClasse, algoritmo[tipoClasse]);
        }
            return classe;
    }
    
   
    
}
