/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package escalonadores;

import java.util.Comparator;

/**
 *
 * @author Carolini
 */
public class Processo {
     int nome;
     int chegada;
     int cpuBusrt;
     int prioridade;
     int ultimaExecucao;
     int cpuBusrt_copia;
     int Nclasse;
     Classe classe;

    public Processo(int nome, int chegada, int cpuBusrt, int prioridade, Classe classe, int i) {
        this.nome = nome;
        this.chegada = chegada;
        this.cpuBusrt = cpuBusrt;
        this.prioridade = prioridade;
        this.classe = classe;
        this.Nclasse = i;
    }
    //teste carolini

    public int getNclasse() {
        return Nclasse;
    }

    public void setNclasse(int Nclasse) {
        this.Nclasse = Nclasse;
    }
    
    public int getCpuBusrt_copia() {
        return cpuBusrt_copia;
    }

    public void setCpuBusrt_copia(int cpuBusrt_copia) {
        this.cpuBusrt_copia = cpuBusrt_copia;
    }
    
    public int getUltimaExecucao() {
        return ultimaExecucao;
    }

    public void setUltimaExecucao(int ultimaExecucao) {
        this.ultimaExecucao = ultimaExecucao;
    }

    public Classe getClasse() {
        return classe;
    }

    public void setClasse(Classe classe) {
        this.classe = classe;
    }

    public int getNome() {
        return nome;
    }

    public void setNome(int nome) {
        this.nome = nome;
    }

    public int getChegada() {
        return chegada;
    }

    public void setChegada(int chegada) {
        this.chegada = chegada;
    }

    public int getCpuBusrt() {
        return cpuBusrt;
    }

    public void setCpuBusrt(int cpuBusrt) {
        this.cpuBusrt = cpuBusrt;
    }

    public int getPrioridade() {
        return prioridade;
    }

    public void setPrioridade(int prioridade) {
        this.prioridade = prioridade;
    }
    
    public void executar(int tempo){
            this.cpuBusrt--; // Diminui o tempo total de execucao do processo
            this.ultimaExecucao = tempo; // armazena o tempo da ultima execucao para o calculo posterior do tempo de processamento e execucao
    }

   public static Comparator<Processo> ProChegada = new Comparator<Processo>() {

	public int compare(Processo s1, Processo s2) {
	   int rollno1 = s1.getChegada();
	   int rollno2 = s2.getChegada();

	   return rollno1-rollno2;
   }};

   public static Comparator<Processo> ProcNOme = new Comparator<Processo>() {

	public int compare(Processo s1, Processo s2) {
	   int rollno1 = s1.getNome();
	   int rollno2 = s2.getNome();

	   return rollno1-rollno2;
   }};
   
    
}
