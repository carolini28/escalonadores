/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package escalonadores;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Carolini
 */
public class Classe {
     private int nome;
     private String algoritmo;
     private int quantum;
     private int quantum_copia;

   
     Processo processo;
     Classe inferior;
     Processo procEmExecut;

     List<Processo> fila = new ArrayList<Processo>();
     List<Processo> filaAux = new ArrayList<Processo>();

    

    public Classe(int nome, String algoritmo) {
        this.nome = nome; 
        this.algoritmo = algoritmo;
        procEmExecut = null;
        
    }
    public List<Processo> getFilaAux() {
        return filaAux;
    }

    public void setFilaAux(List<Processo> filaAux) {
        this.filaAux = filaAux;
    }
    
    public int getQuantum_copia() {
        return quantum_copia;
    }

    public void setQuantum_copia(int quantum_copia) {
        this.quantum_copia = quantum_copia;
    }
    
    public Classe getInferior() {
        return inferior;
    }

    public void setInferior(Classe inferior) {
        this.inferior = inferior;
    }
    
    public int getQuantum() {
        return quantum;
    }

    public List<Processo> getFila() {
        return fila;
    }

    public void setQuantum(int quantum) {
        this.quantum = quantum;
    }

    public int getNome() {
        return nome;
    }

    public void setNome(int nome) {
        this.nome = nome;
    }

    public String getAlgoritmo() {
        return algoritmo;
    }

    public void setAlgoritmo(String algoritmo) {
        this.algoritmo = algoritmo;
    }
    
    
    
    public Processo EscolherProcesso(int tempo){
         
           if(this.nome == 0 || this.nome == 1 || this.nome == 3){
               this.processo = ClasseNaoPreemp(tempo);
           }else if(this.nome == 2 || this.nome == 4){
               this.processo = ClassePreemp(tempo);
           }else{
               this.processo = ClasseRR(tempo);
           }
           return this.processo;
    }
    //Classe de algoritmos de escalonamento preempitivo.
    public Processo ClassePreemp(int tempo){
         //Se há processo(s) desta classe esperando para ser executado
        //System.out.println("Executando ClassePreemp");

        if(this.fila.isEmpty() == false){
            //Resgata o primeiro elemento da fila de prontos
            Processo proc = this.fila.get(0);
            //Se o processo chegou ao fim
            if(proc.cpuBusrt == 1){
                this.fila.get(0).setUltimaExecucao(tempo);
                filaAux.add(this.fila.get(0));
                this.fila.remove(0);
            }else{
                this.fila.get(0).cpuBusrt--;
            }
           
            return proc;
        }else{
               // System.out.println("menor classe1");
                //Verifica e escolhe um processo de uma classe menor prioritária
                if(this.inferior != null){
                    //System.out.println("menor classe2");
                   return this.inferior.EscolherProcesso(tempo);
                }else{
                   return null;
                }
        }
    }
    
    //Classe de algoritmos de escalonamento não preempitivo.
    public Processo ClasseNaoPreemp(int tempo){
         //Se não há processo seu sendo executado
        //System.out.println("Executando ClasseNaoPreemp");
        if(this.procEmExecut == null){
            if(this.fila.isEmpty() == false){
                //Resgata um ponteiro para o primeiro elemento da fila de prontos
                  this.procEmExecut = this.fila.get(0);
                 //Retira este elemento da fila
                  this.fila.remove(0);                      
            }else{
                //Verifica e escolhe um processo de uma classe menor prioritária
                if(this.inferior != null){
                    return this.inferior.EscolherProcesso(tempo);
                 }else{
                    return null;
                 }
            }
        }       
         Processo aux = this.procEmExecut;
         //Se o processo chegou ao fim, sai da execução
         if(this.procEmExecut.getCpuBusrt() == 1){
                this.procEmExecut.setUltimaExecucao(tempo);
                filaAux.add(this.procEmExecut);
                this.procEmExecut = null;
          }else{
             this.procEmExecut.cpuBusrt--;
         }
         
         return aux;
    }
    //Classe escalonadore que utilizam o algoritmo Round-Robin.
    public Processo ClasseRR(int tempo){
       // Se a contagem do quantum chegou ao fim, tira o processo de execução
       // System.out.println("Executando ClasseRR");
        if(this.quantum_copia == 0){
            //Restalrar quantum
            this.quantum_copia = this.quantum;
            //Inserir o processo da fila de prontos
                if(this.procEmExecut != null){
                     // System.out.println("RR = null");
                      this.receberProcesso(this.procEmExecut);
                }
           
            this.procEmExecut = null;
        }
        
       
        
            if(this.procEmExecut == null){
                if(this.fila.isEmpty() == false){
                    //Resgata um ponteiro para o primeiro elemento da fila de prontos
                      this.procEmExecut = this.fila.get(0);
                     //Retira este elemento da fila
                      this.fila.remove(0);                      
                }else{
                    //Verifica e escolhe um processo de uma classe menor prioritária
                    if(this.inferior != null){
                        return inferior.EscolherProcesso(tempo);
                     }else{
                          return null;
                     }
                }
            }

             Processo aux = this.procEmExecut;
             //Se o processo chegou ao fim, sai da execução
             if(this.procEmExecut.getCpuBusrt() == 1){
                    this.procEmExecut.setUltimaExecucao(tempo);
                    filaAux.add(this.procEmExecut);
                    this.procEmExecut = null;
              }else{
                 this.procEmExecut.cpuBusrt--; 
             }
             this.quantum_copia--;
             return aux;

    }
    
    
    public boolean comparador(Processo p1, Processo p2){
        /**/
     if(p1 != null && p2 !=null){   
         //System.out.println("Existem mais de um processoa na fila da classe:"+this.nome); 
        //0 - First Come First Served (FCFS)      
        if(this.nome == 0){ //0 - First Come First Served (FCFS)            
           if(p1.getUltimaExecucao() == p2.getUltimaExecucao()){
               if(p1.getCpuBusrt() == p2.getCpuBusrt()){
                   if(p1.getNome() < p2.getNome()){
                       return true;
                   }else{
                       return false;
                   }
               }else if(p1.getCpuBusrt() < p2.getCpuBusrt()){
                   return true;
               }else{
                   return false;
               }
           }else if(p1.getChegada() < p2.getChegada()){
               return true;
           }else{
               return false;
           }
           //1 - Shortest Job First (SJF)   || 2 - Shortest Remaining Time First (SRTF) 
        }else if(this.nome == 1 || this.nome == 2){//1 - Shortest Job First (SJF)   || 2 - Shortest Remaining Time First (SRTF)        
              
            if(p1.getCpuBusrt() == p2.getCpuBusrt()){
                     if(p1.getChegada() == p2.getChegada()){
                        if(p1.getNome() < p2.getNome()){
                            return true;
                        }else{
                            return false;
                        }
                     }else if(p1.getChegada() < p2.getChegada()){
                             return true;
                     }else{
                             return false;
                     }

                }else if(p1.getCpuBusrt() < p2.getCpuBusrt()){
                        return true;
                }else{
                      return false;
                }
          //3 - Escalonamento por Prioridades || 4 - Escalonamento por Prioridades Preemptivo
        }else if(this.nome == 3 || this.nome == 4){//3 - Escalonamento por Prioridades || 4 - Escalonamento por Prioridades Preemptivo
          // System.out.println(" Escalonamento por Prioridades Preemptivo");
            if(p1.getPrioridade() == p2.getPrioridade()){
               if(p1.getCpuBusrt() == p2.getCpuBusrt()){
                   if(p1.getUltimaExecucao() == p2.getUltimaExecucao()){
                        if(p1.getNome() < p2.getNome()){
                            return true;
                        }else{
                            //System.out.println(" Escalonamento por Prioridades Preemptivo nome");
                            return false;
                        }
                   }else if(p1.getUltimaExecucao() < p2.getUltimaExecucao()){
                       return true;
                   }else{
                       //System.out.println(" Escalonamento por Prioridades Preemptivo ultima execução");
                       return false;
                   }
               }else if(p1.getCpuBusrt() < p2.getCpuBusrt()){
                   return true;
               }else{
                  // System.out.println(" Escalonamento por Prioridades Preemptivo 'cpu' "+"cpu 1 = "+ p1.getCpuBusrt() + "cpu2 = "+ p2.getCpuBusrt());
                   return false;
               }
            }else if(p1.getPrioridade() < p2.getPrioridade()){
                return true;
            }else{
                //System.out.println(" Escalonamento por Prioridades Preemptivo " + "p1:"+p1.getPrioridade()+" p2:"+p2.getPrioridade());
                return false;
            }
            
        }else if(this.nome == 5){//5 - Round-Robin. 
            //0 - First Come First Served (FCFS)            
           if(p1.getChegada() == p2.getChegada()){
               if(p1.getCpuBusrt() == p2.getCpuBusrt()){
                   if(p1.getNome() < p2.getNome()){
                       return true;
                   }else{
                       return false;
                   }
               }else if(p1.getCpuBusrt() < p2.getCpuBusrt()){
                   return true;
               }else{
                   return false;
               }
           }else if(p1.getUltimaExecucao() < p2.getUltimaExecucao()){
               return true;
           }else{
               return false;
           }
       }
        return false;
     }else{
        System.out.println("Nao existem mais de um processoa na fila da classe:"+this.nome); 
        return false;
     }
  }
    public void receberProcesso(Processo processo){
        
       this.fila.add(processo);
       boolean continuar = true;
       Processo aux;
      //  System.out.println("classe:"+this.nome+"size:"+fila.size());
//       System.out.print("classe:"+this.nome);
//       for(int x = this.fila.size()-1; x > 0; x--){
//         System.out.println("Fila:"+this.fila.get(x).getPrioridade()); 
    //   }
       for(int x = this.fila.size()-1; x > 0 && continuar == true ; x--){
           if(comparador(this.fila.get(x), this.fila.get(x-1)) == true){
                  aux = this.fila.get(x);
                  this.fila.set(x, fila.get(x-1));
                  this.fila.set(x-1, aux);
                  //System.out.print("Mudou de posicao fila da classe:"+this.nome); 
           }else{
                  continuar = false;
           }
       }
        
    }
    
    public void Imprimir(){
        System.out.println("Classe"+nome+"-"+algoritmo+".\n");
    }

}